#include<iostream>
using namespace std;
int main()
{
    int i,a=0,b=45,n=9;
    float h=5;
    cout<<"\t\t\tSimpson's 3/8\n\n\n";
    cout<<"time    :    0   5   10   15   20   25   30   35   40   45\n";
    cout<<"velocity:    30  24  19   16   13   11   10    8    7    5\n\n";
    int v[]={30,24,19,16,13,11,10,8,7,5};
    n=(b-a)/h;
    cout<<"\n\th="<<h;
    cout<<"\n\n\tFormula Used : 3*h/8((Y0-Y9)+2*(Y3+Y6)+3(Y1+Y2+Y4+Y5+Y7+Y8))\n";
    double ans=((3*h)/8)*((v[0]+v[9])+2*(v[3]+v[6])+3*(v[1]+v[2]+v[4]+v[5]+v[7]+v[8]));
    cout<<"\n\n\tAnswer : "<<ans<<"\n\n\n";
    cout<<"\n\nMade By :Rajan Lal\nRoll No: 40\n\n\n";
}
