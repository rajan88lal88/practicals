#include<iostream>
#include<math.h>
using namespace std;
double func(double x)
{
    return (cos(x)-x*exp(x));
}
void regulaFalsi(double a, double b)
{
    int i=1;
    double e;
    if (func(a) * func(b) >= 0)
    {
        cout << "You have not assumed right a and b\n";
        return;
    }
    double c = a;
    cout<<"\npermisible error : ";
    cin>>e;
    do
    {
        c = (a*func(b) - b*func(a))/ (func(b) - func(a));
        cout<<"\nIteration "<<i++<<"\tx:\t"<<c<<":\tf(x)\t"<<func(c);
        if (func(c)==0)
            break;
        else if (func(c)*func(a) < 0)
            b = c;
        else
            a = c;
    }while(func(c)<-e||func(c)>e);
    cout << "\n\nThe value of root is : " << c;
}
int main()
{
    cout<<"\n\n\tREGULA FALSI\n\nEquation : (cos(x)-x*exp(x))\n\n";
    double a =0, b = 1;
    regulaFalsi(a, b);
    cout<<"\n\nMade By Rajan Lal\nRoll No: 40\n\n";
    return 0;
}
