// Apply Gauss Jordon to find the solution of :
// 5x - 2y + 3z = -1
// -3x + 9y + z = 2
// 2x - y -7z = 3
// Algorithm :
// 1> Create Augumented matrix
// 2> Normalise Augumneted matrix
// Solution is the last column of Normalised Matrix


#include <iostream>
#define R 3
#define C 4
using namespace std;

void print_matrix(float mat[R][C],int row, int col){
	for(int i = 0; i < row; i++ ){
		for(int j = 0; j < col; j++)
			cout<<mat[i][j]<<"\t";
		cout<<"\n";
	}
}


void normalise_matrix(float mat[R][C],int row, int col){
	for(int k = 0; k < row; k++){
		for(int j = col-1;j >= 0;j--)
			mat[k][j] /= mat[k][k];
		for(int i = 0;i < row; i++){
			float factor = mat[i][k] / mat[k][k];
			if( i != k)
				for(int j = 0; j < col; j++ )
					mat[i][j] -= factor * mat[k][j];
		}
	}
}

int row = 3;
int col = 4;
float aug_mat[R][C] = {6,-1,1,13,1,1,1,9,10,1,-1,19};

int main()
{
	cout<<"Given linear Equations : \n";
	cout<<"\t6x - y + z = 13\n";
	cout<<"\tx + y + z = 9\n";
	cout<<"\t10x + y - z = 19\n";
	cout<<"\nMatrix : \n\n";
	print_matrix(aug_mat,row,col);
	cout<<"\n\n\n";
	normalise_matrix(aug_mat,row,col);
	print_matrix(aug_mat,row,col);
	cout<<"\nValue of :";
	cout<<"\n\tX : "<<aug_mat[0][col-1];
	cout<<"\n\tY : "<<aug_mat[1][col-1];
	cout<<"\n\tZ : "<<aug_mat[2][col-1];
	cout<<"\nRajan Lal\tRoll No: 40";
	return 0;
}
