#include <iostream>
#include <cmath>

#define precision 0.00001
using namespace std;
float value_X(float y,float z){
	return (-1+2*y-3*z)/5.0f;
}

float value_Y(float x,float z){
	return (2.0+3*x-z)/9.0f;
}

float value_Z(float x,float y){
	return (2*x-y-3)/7.0f;
}

float x = 0, y = 0, z = 0;

void gauss_seidal(){
	float x_tmp,y_tmp,z_tmp;
	do{
		x_tmp = x;
		y_tmp = y;
		z_tmp = z;
		x = value_X(y,z);
		y = value_Y(x,z);
		z = value_Z(x,y);
	}while(abs(x-x_tmp) > precision && abs(y-y_tmp) > precision && abs(z-z_tmp) > precision );
}

int main(){
	cout<<"Given linear Equations : \n";
	cout<<"\t5x - 2y + 3z = -1\n";
	cout<<"\t-3x + 9y + z = 2\n";
	cout<<"\t2x - y -7z = 3\n";
	gauss_seidal();
	cout<<"\nValue of :";
	cout<<"\n\tX : "<<x;
	cout<<"\n\tY : "<<y;
	cout<<"\n\tZ : "<<z;
	cout<<"\nRajan Lal\tRoll No:40";
	return 0;
}
