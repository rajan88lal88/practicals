#include<iostream>
#include<stdio.h>
#include<conio.h>
#include<process.h>
using namespace std;
int r,c;
void Display(int F[10][10])
{
    int i,j;
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            cout<<F[i][j];
            cout<<"\t";
        }
        cout<<endl;
    }
}
void prod(int A[10][10],int B[10][10])
{
    int C[10][10],i,j,k;;
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            C[i][j]=0;
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            for(k=0;k<c;k++)
                C[i][j]=C[i][j] + A[i][k]*B[k][j];
    Display(C);
}

void add(int A[10][10],int B[10][10])
{
    int i,j;
    int D[10][10];
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            D[i][j]=A[i][j]+B[i][j];
    Display(D);
}
void transpose(int A[10][10],int B[10][10])
{
    int i,j;
    cout<<"\nTranspose Of Matrix A Is:-\n";
    for(i=0;i<c;i++)
    {
        for(j=0;j<r;j++)
        {
            cout<<A[j][i];
            cout<<"\t";
        }
        cout<<endl;
    }
    cout<<"\n\nTranspose Of Matrix A Is:-\n";
    for(i=0;i<c;i++)
    {
        for(j=0;j<r;j++)
        {
            cout<<B[j][i];
            cout<<"\t";
        }
        cout<<endl;
    }
}
int main()
{
    int choice,i,j,A[10][10],B[10][10];
    char ch;
    cout<<"\n\tMatrix Operations\n";
    cout<<"\n\nMade By Rajan Lal\nRoll No: 40\n\n";
    cout<<"\nEnter No.of Rows And Columns of the Matrix :";
    cin>>r>>c;
    cout<<"\nEnter Elements Of 1st Matrix:\n";
    for(i=0;i<r;i++)
    for(j=0;j<c;j++)
    cin>>A[i][j];
    cout<<"\nEnter Elements Of 2nd Matrix:\n";
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            cin>>B[i][j];
    do
    {
        cout<<"\n1. Addition";
        cout<<"\n2. Product";
        cout<<"\n3. Transpose";
        cout<<"\n4. Exit";
        cout<<"\nEnter Your Choice :  ";
        cin>>choice;
        switch(choice)
        {
            case 1: add(A,B);
                    break;
            case 2: prod(A,B);
                    break;
            case 3: transpose(A,B);
                    break;
            case 4: return(0);
                    break;
            default:cout<<"\nWrong Choice\n";
                    break;
        }
        cout<<"\nWant to Perform More Operations:\n";
        cin>>ch;
    }while(ch=='y');
    getch();
}
