#include<iostream>
#include<math.h>
using namespace std;
double func(double x)
{
    return (cos(x)-x*exp(x));
}
double derivFunc(double x)
{
    return -sin(x)-x*exp(x)-exp(x);
}
void newtonRaphson(double x)
{
    double e;
    int i=1;
    cout<<"\nPermissible Error : ";
    cin>>e;
    double h = func(x) / derivFunc(x);
    while (fabs(h) >= e||fabs(h)<=-e)
    {
        h = func(x)/derivFunc(x);
        x = x - h;
        cout<<"\n"<<i++<<" iteration :"<<x;
    }

    cout << "\nThe value of the root is : " << x;
}
int main()
{
    double x = 1;
    cout<<"\n\n\tNEWTON RAPHSON\n\nEquation : (cos(x)-x*exp(x))\n\n";
    newtonRaphson(x);
    cout<<"\n\nMade By Rajan Lal\nRoll No: 40\n\n";
    return 0;
}
