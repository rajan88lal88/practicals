#include <iostream>
#include <cmath>

#define a 0
#define b 6
#define h 0.5

using namespace std;
double fx(double x){
  return ((double)exp(x)/(1 + x));
}

double trap_integrate(){
  double area=0,i_sum=0;
  for(double i = a+h;i <= b-h;i += h)
    i_sum += 2*fx(i);
  return (h/2.0f)*( (fx(a)+fx(b)) + i_sum );
}

int
main(){
  cout<<"\n\n\tTrapezoidal Rule\n\tEquation : e^x/(1+x)";
  cout<<"\n\n\tTrapezoidal Rule : "<<trap_integrate()<<endl;
  cout<<"\n\n\tRajan Lal\tRoll No: 40\n\n";
  return 0;
}
