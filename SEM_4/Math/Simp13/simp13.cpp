//apply simpson 1/3 (0 to 6) to integrate e^x /(1+x)
#include <iostream>
#include <cmath>

#define a 0
#define b 6
#define h 0.5

using namespace std;
double fx(double x){
  return ((double)exp(x)/(1 + x));
}

double simp_integrate(){
  double area = 0, i_sum = 0;
  int N = (b-a)/h;
  for(int i = 1;i <=N-1; i++)
  	i_sum += (i%2 != 0) ? 4*fx(a+i*h) : 2*fx(a+i*h);

  area = (h/3)*( (fx(a)+fx(b)) + i_sum);
  return area;
}

int
main(){
  cout<<"\n\n\tSimpson 1/3 Rule\n\tEquation : e^x/(1+x)";
  cout<<"\n\n\tSimpson 1/3 Rule is : "<<simp_integrate()<<endl;
  cout<<"\n\n\tRajan Lal\tRoll No: 40\n\n";
  return 0;
}
