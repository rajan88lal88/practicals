#include <iostream>
#include <cmath>

// Value of y(x) at x = 0
#define X0 0
#define Y0 1

using namespace std;

// Functions returning the value of differentiation of fx
double f1x(float x,float y){
	return x + pow(y,2);
}

double f2x(float x, float y){
	return 1 + 2*y*f1x(x,y);
}

double f3x(float x, float y){
	return 2*pow(f1x(x,y),2) + y*f2x(x,y);
}

double f4x(float x, float y){
	return 5*f1x(x,y)*f2x(x,y) + y*f3x(x,y);
}

// Solution of f(x) using Taylor's Method
double fx(float X){
	return (Y0 + (X - X0)*f1x(X0,Y0) + pow((X - X0),2)*f2x(X0,Y0)/2.0f
	        + pow((X - X0),3)*f3x(X0,Y0)/6.0f + pow((X - X0),4)*f4x(X0,Y0)/24.0f);
}

int main(){
	float x;
	cout<<"\n\tTaylor Series\n";
	cout<<"\tEquation : \n";
	cout<<"\tdy/dx = x + y*y\n\n";
	cout<<"\tGiven: y=1 for x=0";
	cout<<"\n\n\tValue of Y(x) at x=0.2  : "<<fx(0.2)<<"\n\n";
	cout<<"\n\n\tRajan Lal\n\tRoll No: 40\n\n";
}
