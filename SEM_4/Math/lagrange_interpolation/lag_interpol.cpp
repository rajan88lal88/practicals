// Lagrange's Interpolation
#include <iostream>
# define N 3
using namespace std;
int Fx(int x){
	switch(x){
		case 1: return 10;
		case 4: return 15;
		case 7: return 9;
	}
}
int X[N] = { 1,4,7 };
float lag_interpolation(float x){
	float sum = 0;
	for(int i = 0;i < N; i++){
		float prod = 1;
		for(int j = 0; j < N ; j++)
			prod = (j != i) ? prod * (float)(x - X[j]) / (X[i] - X[j]) : prod;
		sum += prod * Fx(X[i]);
	}
	return sum;
}
int main(){
	float x;
	cout<<"X:\t\t1\t4\t7\n";
	cout<<"Y:\t\t10\t15\t9\n";
	cout<<"X :";
	cin>>x;
	cout<<"Y at "<<x<<" : "<<lag_interpolation(x)<<endl;
	cout<<"\nRajan Lal\tRoll No: 40";
	return 0;
}
