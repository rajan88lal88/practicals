
// polymorphism_runtime
#include <iostream>
#include<conio.h>
using namespace std;

class polymorphism_runtime
{
	public:
	void func(int x){
		cout << " Value of x is " << x << endl;
	}
	void func(double x){
		cout << " Value of x is " << x << endl;
	}
	void func(int x, int y){
		cout << " Value of x and y is " << x << ", " << y << endl;
	}
};
int main()
{
	polymorphism_runtime o1;
	o1.func(10);
	o1.func(15.5);
	o1.func(20,30);
	cout<<"\n Rajan Lal \tRoll No: 40"<<endl;
	getch();
	return 0;
}
