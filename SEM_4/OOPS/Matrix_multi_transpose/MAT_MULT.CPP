#include<iostream>
#include<conio.h>
using namespace std;
class matrix
{
	int r,c,m;
	int a[10][10], b[10][10], z[10][10];
	public: void getdata();
		void solve();
		void display(char);
};
int main()
{
    char ch;
	matrix a;
	a.getdata();
	cout<<"\nSolve For \n\t1.Multiply\n\t2.Transpose";
	ch=getch();
	if(ch=='1')
    {
        a.solve();
        a.display('1');
    }
    else
        a.display('2');
	getch();
}
void matrix::getdata()
{
	int i,j;
	cout<<"Enter no. of rows in 1st matrix : ";
	cin>>r;
	cout<<"Enter no. of columns in 1st matrix : ";
	cin>>m;
	cout<<"Enter no. of columns in 2st matrix : ";
	cin>>c;
	cout<<"\n\n"<<r<<m<<c<<endl;
	cout<<"Enter First Matrix :\n";
	for(i=0;i<r;i++)
		for(j=0;j<m;j++)
			cin>>a[i][j];
	cout<<"Enter Second Matrix :\n";
	for(i=0;i<m;i++)
		for(j=0;j<c;j++)
			cin>>b[i][j];
}
void matrix::solve()
{
	int i,j,k;
	for(i=0;i<r;i++)
		for(j=0;j<c;j++)
			z[i][j]=0;
	for(i=0;i<r;i++)
		for(j=0;j<c;j++)
			for(k=0;k<m;k++)
				z[i][j]+=a[i][k]*b[k][j];
}
void matrix::display(char t)
{
	int i,j;
	if(t=='1')
	{
	    cout<<"\n\nMultiplication : \n";
        for(i=0;i<r;i++)
        {
            for(j=0;j<c;j++)
                cout<<z[i][j]<<"\t";
            cout<<endl;
        }
	}
	else
    {
        cout<<"\n\nTranspose : 1\n";
        for(i=0;i<m;i++)
        {
            for(j=0;j<r;j++)
                cout<<a[j][i]<<"\t";
            cout<<endl;
        }
        cout<<"\n\nTranspose : 2\n";
        for(i=0;i<c;i++)
        {
            for(j=0;j<m;j++)
                cout<<b[j][i]<<"\t";
            cout<<endl;
        }
    }
}
