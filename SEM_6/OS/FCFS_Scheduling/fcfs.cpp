#include<iostream>
using namespace std;
int main()
{
	int i,n,p[100],wsum=0,tsum=0,ta,wt;
	cout<<"Enter No. of Processes  :";
	cin>>n;
	cout<<"Enter burst time of all the "<<n<<" processes \n";
	p[0]=0;
	for(i=1;i<=n;i++)
		cin>>p[i];
	wt=p[0];
	cout<<"Process \tBurst Time\tWaiting Time\tTurn-Around Time";
	for(i=1;i<=n;i++)
	{
		ta=wt+p[i];
		cout<<"\nP"<<i<<"\t\t" <<p[i]<<"\t\t"<<wt<<"\t\t"<<ta;
		wsum+=wt;
		tsum+=ta;
		wt+=p[i];
	}
	cout<<"\nAverage waiting time : "<<float(wsum)/n;
	cout<<"\nAverage Turn Around Time : "<<float(tsum)/n<<"\n";
	return 0;
}
