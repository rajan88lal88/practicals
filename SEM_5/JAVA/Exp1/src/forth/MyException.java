package forth;
class MyException extends Exception  
{  
    public MyException(String s)  
    {  
    	super(s);  
    }    
    public static void main(String args[])  
    {   
    	
    	try 
        {  
    		System.out.println("Calling MyException with Exception Occured as argument");
        	throw new MyException("Exception Occured");          
        }  
        catch (MyException ex)  
        {  
            System.out.println("Caught");   
            System.out.println(ex.getMessage());  
        }  
        Examp obj = new Examp(); 
	 	try
	 	{ 
	 		System.out.println("\nInside try block");
	 		System.out.println("Dividing a number by 0");
	 		System.out.println(obj.division(15,0));   
	 	} 
	 	catch(ArithmeticException e){ 
	 		System.out.println("\nInside Caught Block");
	 	   System.out.println("You shouldn't divide number by zero\n"); 
	 	}  
	 	finally
	 	{
	 		System.out.println("Inside finally block");
	 		System.out.println("finally block is executed");
	 	}   
    }
} 
