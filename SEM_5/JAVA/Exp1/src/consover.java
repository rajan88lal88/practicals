import java.util.Scanner;
class Box 
{ 
    double width, height, depth; 
    Box(double w, double h, double d) 
    { 
        width = w; 
        height = h; 
        depth = d; 
    } 
    Box() 
    { 
        width = height = depth = 0; 
    } 
    Box(double len) 
    { 
        width = height = depth = len; 
    } 
    double volume() 
    { 
        return width * height * depth; 
    } 
} 
  
public class consover
{ 
    public static void main(String args[]) 
    { 
        Box box1 = new Box(5, 10, 20); 
        Box box2 = new Box(); 
        Box cube = new Box(10); 
        double vol; 
        vol = box1.volume(); 
        System.out.println(" Contructor with 3 arguments\n");
        System.out.println(" \tWidth of box1 is " + box1.width); 
        System.out.println(" \tHeight of box1 is " + box1.height); 
        System.out.println(" \tDepth of box1 is " + box1.depth); 
        System.out.println(" \tVolume of box1 is " + vol+"\n"); 
        vol = box2.volume();
        System.out.println(" Contructor with no arguments\n");
        System.out.println(" \tWidth of box2 is " + box2.width); 
        System.out.println(" \tHeight of box2 is " + box2.height); 
        System.out.println(" \tDepth of box2 is " + box2.depth); 
        System.out.println(" \tVolume of box2 is " + vol + "\n"); 
        vol = cube.volume(); 
        System.out.println(" Contructor with 1 arguments\n");
        System.out.println(" \tWidth of cube is " + cube.width); 
        System.out.println(" \tHeight of cube is " + cube.height); 
        System.out.println(" \tDepth of cube is " + cube.depth); 
        System.out.println(" \tVolume of cube is " + vol + "\n"); 
    } 
} 