import java.util.Scanner; 
public class typecasting
{ 
    public static void main(String[] args) 
    { 
    	System.out.print("Enter a number to typecast : ");
    	Scanner sc = new Scanner(System.in); 
    	double d ;       d=sc.nextDouble();       
    	long l = (long)d;  //explicit type casting required 
    	int i = (int)l; 
    	byte b=(byte)i;//explicit type casting required 
 
    	System.out.println("Double value "+d); 
    	System.out.println("Long value "+l); 
    	System.out.println("Int value "+i); 
    	System.out.println("Byte value "+b); 
    } 
} 
