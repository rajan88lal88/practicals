clc
close all
clear all
f1=input("Enter Carrier Frequency :");
f2=input("Enter Pulse frequency   :");
a=5
t=0:0.005:1
x=a*sin(2*pi*f1*t)                  %Sine Wave
u=(a/2)*square(2*pi*f2*t)+(a/2)     %Square Wave
v=(x.*u)/a                          %product of Sine wave and square wave (ASK)
subplot(311)
plot(t,x);                          %plotting Sine Wave
xlabel("Time")
ylabel("Amplitude")
title("Carrier Wave")

subplot(312)
plot(t,u);                          %plotting Square Wave
xlabel("Time")
ylabel("Amplitude")
title("Square Wave")

subplot(313)
plot(t,v);                          %Plotting ASK
xlabel("Time")
ylabel("Amplitude")
title("ASK")
