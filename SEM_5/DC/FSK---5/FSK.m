clc;
close all;
clear all;
f1=input("Enter 1's Frequency :");
f2=input("Enter 0's frequency :");
a=5;
t=0:0.00005:1;
x=a*sin(2*pi*f1*t);               %Sine Wave;
x1=a*sin(2*pi*2*f1*t);
u=(a/2)*square(2*pi*f2*t) + a/2;       %Square Wave
u1=(a/2)*square(2*pi*f2*t+pi) + a/2;  
v=(x.*u)/a+(x1.*u1)/a;                          %product of Sine wave and square wave (ASK)
subplot(411);
plot(t,x);  %plotting Sine Wave
grid on
ax = gca;
ax.GridColor ='black';
ax.GridAlpha = 0.5;

xlabel("Time");
ylabel("Amplitude");
title("Carrier Wave");

subplot(412);
plot(t,x1);                          %plotting Sine Wave
grid on
ax = gca;
ax.GridColor ='black';
ax.GridAlpha = 0.5;

xlabel("Time");
ylabel("Amplitude");
title("2*Frequency");


subplot(413);
plot(t,u);                          %plotting Square Wave
grid on
ax = gca;
ax.GridColor ='black';
ax.GridAlpha = 0.5;

xlabel("Time");
ylabel("Amplitude");
title("Square Wave");

subplot(414);
plot(t,v);                         %Plotting ASK
grid on
ax = gca;
ax.GridColor ='black';
ax.GridAlpha = 0.5;

xlabel("Time");
ylabel("Amplitude");
title("FSK");
