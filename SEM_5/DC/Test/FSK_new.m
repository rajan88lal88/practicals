clc;
close all;
clear all;
f1=input("Enter 1's Frequency :");
f2=input("Enter 0's frequency :");
a=1;
t=0:0.001:1;
x=a*sin(2*pi*f1*t);
subplot(411);
plot(t,x);
xlabel("Time");
ylabel("Amplitude");
title("Carrier Wave");
u=(a/2)*square(2*pi*f2*t) + a/2;
subplot(412);
plot(t,u);                          %plotting Square Wave
xlabel("Time");
ylabel("Amplitude");
title("Square Wave");
m=a*square(2*pi*f1*t)

for(i=0:1000)
    if(u(i+1)==0)
        m(i+1)=sin(2*pi*f1*t(i+1));
    else
        m(i+1)=sin(4*pi*f1*t(i+1));
    end;
end;
subplot(413);
plot(t,m);                          %plotting Square Wave
xlabel("Time");
ylabel("Amplitude");
title("FSK");