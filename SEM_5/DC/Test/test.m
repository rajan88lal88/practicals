clc;
close all;
clear all;
f1=input("Enter 1's Frequency :");
f2=input("Enter 0's frequency :");
a=1;
t=0:0.01:20;
x=a*sin(2*pi*f1*t);
subplot(411);
plot(t,x);
xlabel("Time");
ylabel("Amplitude");
title("Carrier Wave");

n=20;
s=rand(1,n)>0.5;
s=repmat(s',1,100)'
s=s(:)'
t=linspace(0,n,numel(s))
subplot(412);
plot(t,s,'r')

m=a*square(2*pi*f1*t)

for(i=0:1999)
    if(s(i+1)==0)
        m(i+1)=sin(2*pi*f1*t(i+1));
    else
        m(i+1)=sin(4*pi*f1*t(i+1));
    end;
end;

subplot(413);
plot(t,m,'g');
