clc;
clear all;
close all;
a=4;
bits=3;                         %No. of total bits
f=1;
fs=50;                          %sampling Frequency
t=0:1/fs:2*pi;              
y=a*sin(2*pi*f*t);              %Original Sine Wave
ns=length(y);
q_out=zeros(1,ns);
del=(2*a)/(2^bits);             %value of delta for given signal
l=-a+(del/2);                   %Lower Limit of a level
h=a-(del/2);                    %higher limit of a level
    for i=l:del:h
        for j=1:ns
            if(((i-del/2)<y(j))&&(y(j)<(i+del/2)))
                q_out(j)=i;
            end
        end
    end
    stem(t,q_out);              %plotting Quantised Signal
    hold on;
    plot(t,y,'r');              %plotting sine wave
    xlabel("Time")
    ylabel("Amplitude")
    title("Quantization of Signal")
            