clc                         %clears the command window
close all                   %close all opened windows
clear all                   %clear previous program data

%Message Signal

f=100                       %Frequency of message signal
t=0:0.00005:0.05            %time Interval
y=cos(2*pi*f*t)             %Amplitude
subplot(411)                %Graph position and dimension
plot(t,y)                   %Plotting the above values of y and t
xlabel("Time")              %Giving label to x-axis
ylabel("Amplitude")         %Giving Label to y-axis
title("Signal")             %Giving title to the graph

%Under Sampling

f1=1.65*f                   %Under Sampling (Sampling frequency < 2* Message Frequency) 
t1=0:1/f1:0.05              %new time interval= 1/sampling frequency
y1=cos(2*pi*f*t1)           %Amplitude
subplot(412)                %Graph position and dimension
plot(t,y,'b',t1,y1,'r*-')   %Plotting 2 new and original signal on same graph
xlabel("Time")              %Giving label to x-axis
ylabel("Amplitude")         %Giving Label to y-axis
title("Under Sampling")     %Giving title to the graph

%Normal Sampling

f2=2*f
t2=0:1/f2:0.05
y2=cos(2*pi*f*t2)
subplot(413)
plot(t,y,'b',t2,y2,'r*-')
xlabel("Time")
ylabel("Amplitude")
title("Normal Sampling")

%Over Sampling

f3=4*f
t3=0:1/f3:0.05
y3=cos(2*pi*f*t3)
subplot(414)
plot(t,y,'b',t3,y3,'r*-')
xlabel("Time")
ylabel("Amplitude")
title("Over Sampling")

